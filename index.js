// console.log("Hello World");

// 1. Without using the array methods. Create a function that will accept 5 numbers from the user input and store it in an array. After storing the five numbers, calculate the sum of the five numbers and display the result using alert.

function toGetNumbers(){
    let num1 = Number(prompt("Enter first number:"));
    let num2 = Number(prompt("Enter second number:"));
    let num3 = Number(prompt("Enter third number:"));
    let num4 = Number(prompt("Enter fourth number:"));
    let num5 = Number(prompt("Enter five number:"));

    const numbers = [num1, num2, num3, num4, num5];
    // console.log(numbers);
    let sumOfFiveNumbers = num1 + num2 + num3 + num4 + num5;
    alert(`The total of the entered five numbers is ${sumOfFiveNumbers}.`);
}
// ==============================================




// 2. Create a function that will look for the duplicate elements in an array.
/* 
    Expected output: 
    Duplicate fruits found:
    ["apple", "grapes"]
*/


let fruits = ["apple", "grapes", "orange", "mango", "apple", "strawberry", "grapes"];

function toGetDuplicateItems(){
    // 'Set' is a special data structure introduced in ES6 that stores a collection of unique values. Each value has to be unique so passing any duplicate item will be removed automatically.
    let newFruits = new Set(fruits); 
    // console.log(newFruits); //return new set of array

    let duplicates = fruits.filter((item, index) => index !== fruits.indexOf(item));
    console.log(duplicates);
}
// ================================================






/* 
    3. Create an array of numbers that will contain the following values: 
        let numbers = [2, 4, 6, 8, 10];

    - Use the map method to compute for the cube of each array element.

    - Using a for loop display each element of the array with their equivalent cube result

    Expected output:
    1. The cube value of 2 is 8.
    2. The cube value of 4 is 64.
    3. The cube value of 6 is 216.
    4. The cube value of 8 is 512.
    5. The cube value of 10 is 1000.

*/


/* To get input data (number) from the user --------

    for (i = 0; i <= 4; i++){
        let givenNumber = Number(prompt("Give me a number:"));
        let numberCube = Math.pow(givenNumber, 3);
        console.log(`The cube value of ${givenNumber} is ${numberCube}.`);
    } 
*/

let numbers = [2, 4, 6, 8, 10];

for (let i = 0; i < numbers.length; i++){
    console.log(`The cube value of ${numbers[i]} is ${Math.pow(numbers[i], 3)}.`)
}


// To print the cube of array variable 'numbers' ---------
// console.log(numbers.map(number => Math.pow(number, 3))); 